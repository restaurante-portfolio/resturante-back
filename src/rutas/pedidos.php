<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//get all customers
$app->get('/api/pedidos', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT * FROM `pedidos` WHERE 	estado_pedido = 'PENDIENTE' ORDER BY id DESC";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id'];  
        }

        return $resultado ?: [];
    }, $response);
});

$app->get('/api/allorders', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT * FROM `pedidos` WHERE 	estado_pedido <> 'PAGADO' AND estado_pedido <> 'CANCELADO' ORDER BY id DESC";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id'];  
        }

        return $resultado ?: [];
    }, $response);
});

$app->get('/api/pedidoskitchen', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT * FROM `pedidos` WHERE 	estado_pedido = 'PREPARACION' ORDER BY id ASC";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id'];  
        }
        return $resultado ?: [];
    }, $response);
});

$app->get('/api/detailspedidoskitchen', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT det.sku_plato AS sku, det.id AS id, det.id_pedido AS id_pedido, pla.nombre_plato AS plato, pla.valor_plato AS precio, det.cantidad_plato AS qty FROM detalle_pedido AS det 
        INNER JOIN pedidos AS ped
        INNER JOIN platos AS pla
        ON det.id_pedido = ped.id_pedido AND det.sku_plato = pla.sku_plato
        WHERE ped.estado_pedido <> 'PAGADO' AND ped.estado_pedido <> 'CANCELADO' ORDER BY ped.id ASC";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        if ( $resultado) {
            foreach ($resultado as $i=>$registro)  {
                $resultado[$i]['id'] = (int)$registro['id'];  
            }
        }
        return $resultado ?: [];
    }, $response);
});

$app->get('/api/pedidos-last-created', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $sql =  "SELECT MAX(id) AS ultimo FROM pedidos";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);

});

//create new customer
$app->post('/api/pedidos/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        function resultRes($plate){
             $sql =  "SELECT * FROM pedidos WHERE id_pedido = '$plate'";
             $dbConexion = new DBConexion(new Conexion());
             $resultado = $dbConexion->executeQuery($sql);
             return $resultado;
        }

          //throw new Exception('malo');
          $params = $request->getParams(); 

            $sql = "INSERT INTO pedidos (id, numero_mesa, id_pedido, nombre_empleado, direccion_pedido, observacion_pedido, estado_pedido, totalOrder) VALUES 
            (NULL,:numero_mesa, :id_pedido,:nombre_empleado,:direccion_pedido, :observacion_pedido, :estado_pedido, :totalOrder)";
            $dbConexion = new DBConexion(new Conexion());
         
        $resultado = $dbConexion->executePrepare($sql, $params);
        $resultado = (object)  resultRes($params['id_pedido']);

        return $resultado ?: [];
      }, $response);
  });

  $app->post('/api/detailspedidos/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        
        $params = $request->getParams(); 
        foreach ($params as $key => $value) {
            $sql = "INSERT INTO detalle_pedido (id, id_pedido, sku_plato, cantidad_plato) VALUES 
            (NULL, :id_pedido, :sku_plato, :cantidad_plato)";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executePrepare($sql, $value);
        }
        return $resultado ?: [];
      }, $response);
  });


  //update all information for customer
$app->put('/api/pedidos/update', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE pedidos SET 
        numero_mesa = :numero_mesa,
        id_pedido = :id_pedido,
        direccion_pedido = :direccion_pedido,
        observacion_pedido = :observacion_pedido WHERE id_pedido = :id_pedido";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

 $app->put('/api/pedidos/estado', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE pedidos SET 
        estado_pedido = :estado_pedido WHERE id_pedido = :id_pedido";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

?>