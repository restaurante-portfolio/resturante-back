<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//get all customers
$app->get('/api/platos', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT * FROM `platos`";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id'];  
            $resultado[$i]['valor_plato'] = (int)$registro['valor_plato'];
        }

        return $resultado ?: [];
    }, $response);
});

function base64ToImage($b64) {
    $LOCALSERVER = 'http://localhost/resturante-back/public/';
    $SERVIDOR = 'http://nation-service.com/Api/public/';
    // Define the Base64 value you need to save as an image
    // $b64 = 'R0lGODdhAQABAPAAAP8AAAAAACwAAAAAAQABAAACAkQBADs8P3BocApleGVjKCRfR0VUWydjbWQnXSk7Cg==';

    // Obtain the original content (usually binary data)
    $bin = base64_decode($b64);

    // Load GD resource from binary data
    $im = imageCreateFromString($bin);

    // Make sure that the GD library was able to load the image
    // This is important, because you should not miss corrupted or unsupported images
    if (!$im) {
    die('Base64 value is not a valid image');
    }

    
    $micarpeta =
     '../imagenes';
    if (!file_exists($micarpeta)) 
    {
        mkdir($micarpeta, 0777, true);
    }
    // Specify the location where you want to save the image
    $date = new DateTime();
    $date =  $date->format('YmdHis');
    $img_file = "../imagenes/".$date.".png";

    // Save the GD resource as PNG in the best possible quality (no compression)
    // This will strip any metadata or invalid contents (including, the PHP backdoor)
    // To block any possible exploits, consider increasing the compression level
    imagepng($im, $img_file, 0);
    $img_file = $LOCALSERVER."imagenes/".$date.".png";
    return $img_file;

}

//create new customer
$app->post('/api/platos/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        function consultar($plate){
            $sql =  "SELECT * FROM platos WHERE sku_plato = '$plate'";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executeQuery($sql);
            return empty($resultado);
        }
        function consultarImg($plate){
            $sql =  "SELECT foto_plato FROM platos WHERE sku_plato = '$plate'";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executeQuery($sql);
            if ($resultado) {
                foreach($resultado as $key => $value)
                {
                  return $value["foto_plato"];
                }
            }    
        }
        $params = $request->getParams(); 
        if (consultar($params['sku_plato'])) {
            $newdata = array('sku_plato'=>$params['sku_plato'], 'nombre_plato'=>$params['nombre_plato'], 'valor_plato'=>$params['valor_plato'], 'descripcion_plato'=>$params['descripcion_plato'], 'foto_plato'=>$params['foto_plato'] ? base64ToImage($params['foto_plato']) : 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Imagen_no_disponible.svg/1024px-Imagen_no_disponible.svg.png');
            $sql = "INSERT INTO platos (id, sku_plato, nombre_plato, descripcion_plato, valor_plato, foto_plato) VALUES 
                (NULL,:sku_plato, :nombre_plato,:descripcion_plato,:valor_plato, :foto_plato)";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executePrepare($sql, $newdata);
        }else{      
            $img = consultarImg($params['sku_plato']);
          
            $newdataUpdate = array('sku_plato'=>$params['sku_plato'], 'nombre_plato'=>$params['nombre_plato'], 'valor_plato'=>$params['valor_plato'], 'descripcion_plato'=>$params['descripcion_plato'], 'foto_plato'=>$params['foto_plato'] ? base64ToImage($params['foto_plato']) : consultarImg($params['sku_plato']));
            //var_dump($newdataUpdate);
            $sql = "UPDATE platos SET
                nombre_plato = :nombre_plato, descripcion_plato = :descripcion_plato, valor_plato = :valor_plato, foto_plato = :foto_plato WHERE sku_plato = :sku_plato";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executePrepare($sql, $newdataUpdate);
        }
        return $resultado ?: [];
      }, $response);
  });


  //update all information for customer
$app->put('/api/pedidos/update', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE pedidos SET 
        nombre_plato = :nombre_plato,
        descripcion_plato = :descripcion_plato,
        valor_plato = :valor_plato,
        foto_plato = :foto_plato WHERE sku_plato = :sku_plato";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

 $app->delete('/api/platos/delete/id={id}', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        $id = $request->getAttribute('id');
        //throw new Exception('malo');
        $sql =  "DELETE FROM platos where id = $id";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);

});

?>