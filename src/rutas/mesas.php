<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//get all customers
$app->get('/api/mesas', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT * FROM `mesas`";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});

//create new customer
$app->post('/api/mesas/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
          //throw new Exception('malo');
          $params = $request->getParams(); 

            $sql = "INSERT INTO mesas (id, numero_mesa) VALUES 
            (NULL,:numero_mesa)";
            $dbConexion = new DBConexion(new Conexion());
         
        $resultado = $dbConexion->executePrepare($sql, $params);
        return $resultado ?: [];
      }, $response);
  });



 $app->put('/api/pedidos/estado', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE pedidos SET 
        estado_pedido = :estado_pedido WHERE id_pedido = :id_pedido";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

 $app->delete('/api/mesas/delete/id={id}', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        $id = $request->getAttribute('id');
        //throw new Exception('malo');
        $sql =  "DELETE FROM mesas where id = $id";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);

});

?>